# Fedi Ponies
🐴 Kickstarting ponies on Fediverse.

`fediponies` is the official repository of the site that is used to help promote the Fediverse for the My Little Pony fandom. Feel free to fork and contribute.

## Licenses
Code under the Fedi Ponies project is licensed under [GNU General Public License 3.0](LICENSE)<!--, while all the content is licensed under [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International](LICENSE_CONTENT)-->.

## Contributing
1. Install [Deno](https://deno.land/manual/getting_started/installation) on your machine.
2. To show results in realtime, run `deno task serve` and access the URL given with a browser.
3. To build the project, run `deno task lume`.

## Dependencies
* [Deno](https://deno.land)
* [ESBuild](https://esbuild.github.io)
* [Lume](https://lume.land)

## Credits
* Inspired by [The Furry Fediverse](https://furryfediverse.org/).
* Colour system depends on [Lch Colour Picker](https://css.land/lch/).