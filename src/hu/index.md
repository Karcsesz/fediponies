---
lang: hu
id: index
url: /
title: Fedi Ponies
contributors:
  - Karcsesz
---
## Vágjunk bele!
### Mi az a Fedi Ponies?
A **Fedi Ponies** egy Fediverzum-integrált szolgáltatásokból álló, főképp az Én Kicsi Pónim rajongói számára több nyelven fenntartott szolgáltatáscsoport. A **Fedi Ponies** segít, hogy könnyen megtaláld azt a kezdőközösséget, ahol otthont találhatsz a széles és változatos Fediverzumban!

### Mi az a Fediverzum?
A Fediverzum (federált univerzum) egy több külön üzemeltetett, viszont összekötött közösségi platform által alkotott világ. Mindenki a Fediverzumon érintkezhet egymással, kivéve ha ezt külön tiltás akadályozza. Gazdag és burjánzó hálózat, mely számtalan közösséget köt össze.

A Fediverzum egy tarka hálózat. A Twitter-szerű mikroblogokat [Mastodon](https://joinmastodon.org), [Misskey](https://misskey.page), [Firefish](https://joinfirefish.org) és társai teszik lehetővé. A [PixelFed](https://pixelfed.org) Instagramra hajazó képmegosztásra van kihegyezve, a [PeerTube](https://joinpeertube.org) pedig YouTube-féle videómegosztásra. Ezentúl még sok más célú szoftver is csatlakozni tud.

Bár az egyes oldalaknak van tulajdonosa, magát a Fediverzumot nem birtokolja senki, így lehetetlen, hogy akárki átvegye a hálózat felett az irányítást. A vállalatok által üzemeltetett közösségi hálókkal ellentétben a Fediverzum felhasználóinak a birtokában marad minden adatuk. És ha mindez nem elég, nem a maximális profit kifacsarásáért üzemeltetik az oldalakat az adminisztrátorok, és adományokkal tartják fenn magukat, így kifejezetten ritkák a reklámokkal és hirdetésekkel telített portálok.

### Hogyan tudok csatlakozni a Fediverzumhoz?
Kifejezetten egyszerüen! Válassz egy oldalt, majd pedig regisztrálj fiókot. Gyakran szükséges E-mail címet megadni a regisztrációhoz, viszont az ilyenkor kiküldött üzenetek gyakran fennakadnak a levélszemétszűrőn, úgyhogy érdemes a Spam mappát is megnézni. Amint engedélyezik a fiókodat, a színpad a tiéd!

### Tudnom kell angolul?
Erősen ajánlott az angol nyelv használata a Fediverzumon, de sem kötelező, sem elvárt. Szabadon megválaszthatod, milyen nyelven szeretnél szólni.

### Mi van, ha nem tetszik az alapértelmezett felület?
A legtöbb esetben érdemes telepíteni egy alkalmazást. Számtalan kiváló klienssel rendelkeznek a Fediverzum platformok, az egyik biztosan tetszeni fog. A böngészős oldalakhoz szintén letölthetőek témák és kiegészítők, amikkel ki lehet foltozni az esetleges hiányosságokat.

## Hogyan tovább?
Na, mire vársz még? [Válassz egy szervert és csatlakozz a Fediverzumhoz](./next/)!
