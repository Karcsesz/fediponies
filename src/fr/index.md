---
lang: fr
id: index
url: /
title: Poneys sur Fediverse
contributors:
  - Lumière Élevé
---
## Commencez !
### Qu'est-ce que Poneys sur Fediverse ?
**Poneys sur Fediverse** est une collection des instances sur le Fediverse vers le fandom de « My Little Pony », qui est traduit en plusieurs langues pour la commodité de tout le monde. Avec **Poneys sur Fediverse**, choisissez votre instance de rêve comme votre communauté de lancement, et ouvrez la passerelle vers un Fediverse plus large !

### Qu'est-ce que Fediverse ?
Fediverse (« l'univers fédéré », ou « federated universe » en anglais) est un réseau interconnecté de divers réseaux sociaux distincts. Tout le monde sur le Fediverse est en mesure d'interagir les uns avec les autres, si sans autres restrictions appliquées. C'est un ensemble luxuriant et organique d'innombrables communautés.

Fediverse est une plate-forme diversifiée. Pour les microblogging ressemblant à Twitter, il y a [Mastodon](https://joinmastodon.org), [Misskey](https://misskey.page), [Firefish](https://joinfirefish.org) et etc. [PixelFed](https://pixelfed.org) pour le partage d'images ressemblant à Instagram, [PeerTube](https://joinpeertube.org) pour le partage vidéo ressemblant à YouTube, et bien plus encore.

Malgré les instances eux-mêmes, par Fediverse n'appartient pas à une seule entité, empêchant ainsi toute possibilité qu'une entité prenne le relais. Contrairement aux médias sociaux gérés par l'entreprise, les utilisateurs de Fediverse gardent la propriété de leurs propres données. Il ne possède pas non plus de volonté de profit - les instances repose principalement sur des dons, ce qui le rend extrêmement peu susceptible de rencontrer un enfer infesté infesté d'annonces.

### Comment rejoindre le Fediverse ?
C'est très facile ! Choisissez d'abord une instance, puis enregistrez un compte dessus. Les adresses e-mail sont souvent nécessaires pour passer le processus de vérification, mais ces e-mails se retrouvent le plus souvent dans le dossier spam, alors tamisez également votre dossier de spam. Une fois que votre compte est passé à l'admission, bienvenue à la fête !

### Dois-je parler l'anglais ?
Parler l'anglais est encouragé sur le Fediverse, mais ce n'est ni une exigence ni une obligation morale. Vous êtes libre de choisir la langue que vous souhaitez parler. Même en français !

### Et si l'interface par défaut ne se sent pas satisfaisante ?
Surtout, essayez d'installer une application qui correspond à votre goût ! Avec divers excellents clients de Fediverse, on serait certainement votre choix exact. Pour les navigateurs, il existe également des CSS et des extensions dédiées à l'embellissement des interfaces existantes.

## Et après?
Maintenant, qu'attendez-vous ? [Sélectionnez un serveur, et rejoignez le Fediverse](./next/) !
